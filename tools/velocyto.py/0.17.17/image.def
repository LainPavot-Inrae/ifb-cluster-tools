Bootstrap: docker
From: debian:10

%labels
    Author Julien Seiler & J.C. Haessig for IGBMC/BigEst/IFB
    Version master

%environment
  export PATH=/opt/conda/bin:$PATH

%post
    export PATH=/opt/conda/bin:$PATH
    export DEBIAN_FRONTEND=noninteractive
    # prepare debian/ubuntu
    apt-get update && apt-get upgrade -y
    apt-get install -y  \
            apt-utils \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common \
            sed \
            wget \
            git \
            build-essential \
            python3 python3-pip \
            zlib1g-dev \
            bzip2 \
            r-base \
            r-cran-rcpp \
            r-cran-rcppeigen \
            r-cran-rinside \
            r-cran-matrix

    apt-get clean
    apt-get purge


    # install conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    # install velocyto
    conda update -y conda
    . /opt/conda/etc/profile.d/conda.sh
    conda install -c conda-forge -c bioconda -y velocyto.py=0.17.17

    pip install rpy2==2.9.5

%test
    export PATH=/opt/conda/bin:$PATH
    . /opt/conda/etc/profile.d/conda.sh
    velocyto --help

