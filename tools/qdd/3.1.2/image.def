BootStrap: docker
From: registry.gitlab.com/ifb-elixirfr/docker-images/mamba:0-6-4

%labels
    Author IFB
    Version 3.1.2+singularity0

%files
    set_qdd_default.ini /usr/local/qdd/set_qdd_default.ini.template
    get_qdd_default_conf /usr/local/qdd/get_qdd_default_conf

%post
    # dependencies
    export PATH=/opt/conda/bin:$PATH
    mamba install -y -q -c conda-forge -c bioconda -c default perl-bioperl-core blast clustalw primer3==2.4.1a repeatmasker

    # Fix primer3 :
    #   Unable to open file /projet/fr2424/sib/lecorguille/miniconda3/envs/qdd_test/bin/primer3_config/stack.ds
    cd /opt/conda/bin
    ln -s ../share/primer3/primer3_config .

    # source
    mkdir -p /usr/local/qdd
    cd /usr/local/qdd
    wget -q http://net.imbe.fr/~emeglecz/QDDweb/QDD-3.1.2/QDD-3.1.2.tar.gz
    tar -zxf QDD-3.1.2.tar.gz
    rm -f QDD-3.1.2.tar.gz

    # installation
    cp *.pm /opt/conda/lib/site_perl/5.26.2/

    # modification
    sed -i 's,#!/usr/bin/perl,#!/opt/conda/bin/perl,' *.pl
    chmod +x *.pl

    # cleanup
    conda clean -y --quiet --all
    rm -rf ncbi_tax.db *.xml data/example*

    # config
    mkdir /etc/qdd/

%environment
    export PATH=/opt/conda/bin:$PATH
    export PATH=/usr/local/qdd/:$PATH

%test
    # Check dependencies
    ls /opt/conda/bin/primer3_config/
    # Check executables
    export PATH=/opt/conda/bin:$PATH
    export PATH=/usr/local/qdd/:$PATH
    cd /tmp
    get_qdd_default_conf
    ls -l set_qdd_default.ini
    pipe1.pl # | grep "QDD version3.1.2 July 2014"
    pipe2.pl # | grep "QDD version3.1.2 July 2014"
    pipe3.pl # | grep "QDD version3.1.2 July 2014"
    pipe4.pl # | grep "QDD version3.1.2 July 2014"
    QDD.pl #| grep "QDD version3.1.2 July 2014"
    blastn -version | grep blastn
    RepeatMasker | grep RepeatMasker
    primer3_core -h 2>&1 | grep "This is primer3"
    clustalw --ouhaich | grep CLUSTAL
